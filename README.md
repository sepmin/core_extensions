# `__init__.py`
This is required by core network emulator to identify the custom services.

# bind9.py
Adds the bind9 to the services under a new section called "Extensions".
The bind9 service must be installed on the host (for core).
Perhaps there are better ways to do this however at the moment the emulated service expects all the configuration files to be under "/etc/bind.d".
Since apparmor is not going to be happy about this and will prevent the service from working the line `/etc/bind.d/** rwm,` must be added to `/etc/apparmor.d/local/usr.sbin.named`.

# maradns.py
This is a very simple DNS service.

# postfix.py
This probably can be done better (may have had the same issue with apparmor) but at the moment overrides the default directory `/etc/postfix`.
Since there are few configuration files needed by postfix when the service is installed on the host a copy of the default files must be created under `/etc/postfix.d`.
The `postfix.sh` which will set up and run the service will first copy the default configuration files from host to the node directory.
The required changes are then done using `postconf` commands.

# resolveconf.py
A quick way to add DNS server settings to a node.

# strongswan.py
Since all three configuration files namely `ipsec.conf`, `ipsec.secrets`, and `strongswan.conf` allow the use of include to provide a per-node configuration a line must be added to each of the files on the host.
Under the `/etc/ipsec.d` a directory must be created with the name core and each of the aforementioned files will have the corresponding include command.
For instance in `/etc/ipsec.conf` on the host the line `include /etc/ipsec.d/core/*.conf` must be added.
Since strongswan.py creates the `/etc/ipsec.d/core` per-node each node will only see its own configuration.
No configuration must be used on the host.