import os

from core.service import CoreService, addservice
from core.misc.ipaddr import IPv4Prefix, IPv6Prefix
from core.constants import *

class PostfixService(CoreService):
    ''' This is a sample user-defined service.
        Postfix provides smtp mail service.
        This extension is added to Core by Sepehr Minagar.
    '''
    # a unique name is required, without spaces
    _name = "postfix"
    # you can create your own group here
    _group = "Utility"
    # list of other services this service depends on
    _depends = ()
    # per-node directories
    _dirs = ('/var/lib/postfix', '/var/mail', '/var/spool/postfix','/etc/postfix')
    # generated files (without a full path this file goes in the node's dir,
    #  e.g. /tmp/pycore.12345/n1.conf/)
    _configs = ('postfix.sh',)
    # this controls the starting order vs other enabled services
    _startindex = 50
    # list of startup commands, also may be generated during startup
    _startup = ('sh postfix.sh',)
    # list of shutdown commands
    _shutdown = ('postfix stop')

    @classmethod
    def generateconfig(cls, node, filename, services):
        ''' Return a string that will be written to filename, or sent to the
            GUI for user customization.
        '''
        addrlist = ''
        if filename == 'postfix.sh':
            cfg = "#!/bin/sh\n"
            cfg += "# auto-generated for postfix\n"
            cfg += '''
# uncomment and modify the configuration parameters according to your setup
cp -R /etc/postfix.d/* /etc/postfix

postconf -e "mynetworks_style=subnet"

# a comma seperated list of interfaces
#postconf -e "inet_interfaces=127.0.0.1"
#postconf -e "inet_protocols=ipv4"

#postconf -e "myhostname=mail"
#postconf -e "mydomain=example.com"

#postconf -e "myorigin=example.com"

#postconf -e "mydestination=$myhostname, localhost, $myhostname.$mydomain, $mydomain"

postfix start
'''
            
        return cfg


    @staticmethod
    def subnetentry(x):
        ''' Generate a subnet declaration block given an IPv4 prefix string
            for inclusion in the config file.
        '''
        if x.find(":") >= 0:
            # this is an IPv6 address
            return ""
        else:
            net = IPv4Prefix(x)
            return 'echo "  network %s"' % (net)

# this line is required to add the above class to the list of available services
addservice(PostfixService)
