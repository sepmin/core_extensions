import os

from core.service import CoreService, addservice
from core.misc.ipaddr import IPv4Prefix, IPv6Prefix
from core.constants import *

class Bind9Service(CoreService):
    ''' This is a sample user-defined service. 
        Bind9 is a fully fledged DNS service.
        This module extentsion is added to Core by Sepehr Minagar.
    '''
    # a unique name is required, without spaces
    _name = "bind9"
    # you can create your own group here
    _group = "Extensions"
    # list of other services this service depends on
    _depends = ()
    # per-node directories
    _dirs = ('/etc/bind.d','/var/cache/bind', '/ver/run/named')
    # generated files (without a full path this file goes in the node's dir,
    #  e.g. /tmp/pycore.12345/n1.conf/)
    _configs = ('bind.sh', '/etc/bind.d/named.conf', '/etc/bind.d/named.conf.options', '/etc/bind.d/named.conf.local', '/etc/bind.d/named.conf.default-zones', '/etc/bind.d/db.example.com',)
    # this controls the starting order vs other enabled services
    _startindex = 50
    # list of startup commands, also may be generated during startup
    _startup = ('sh bind.sh',)
    # list of shutdown commands
    _shutdown = ('service bind9 stop')

    @classmethod
    def generateconfig(cls, node, filename, services):
        ''' Return a string that will be written to filename, or sent to the
            GUI for user customization.
        '''
        if filename == 'bind.sh':
            cfg = "#!/bin/sh\n"
            cfg += "# auto-generated for bind9\n"
            #for ifc in node.netifs():
                #cfg += '#Node %s has interface %s\n' % (node.name, ifc.name)
                # here we do something interesting 

                #cfg += "\n".join(map(cls.subnetentry, ifc.addrlist))
                #break
             
            cfg += '''
# copying the root database for the virtual node from the host
cp /etc/bind/db.root /etc/bind.d
named -4 -c /etc/bind.d/named.conf
                    '''    
            
        

        if filename == '/etc/bind.d/named.conf':
            cfg = "// Example named.conf file\n"
            cfg += '''
// This is the primary configuration file for the BIND DNS server named.
//
// Please read /usr/share/doc/bind9/README.Debian.gz for information on the 
// structure of BIND configuration files in Debian, *BEFORE* you customize 
// this configuration file.
//
// If you are just adding zones, please do that in /etc/bind/named.conf.local

include "/etc/bind.d/named.conf.options";
include "/etc/bind.d/named.conf.local";
include "/etc/bind.d/named.conf.default-zones";
 
            '''
        


        if filename == '/etc/bind.d/named.conf.options':
            cfg = "// Example named.conf.options file\n"
            cfg += '''
options {
    directory "/var/cache/bind";

    // If there is a firewall between you and nameservers you want
    // to talk to, you may need to fix the firewall to allow multiple
    // ports to talk.  See http://www.kb.cert.org/vuls/id/800113

    // If your ISP provided one or more IP addresses for stable 
    // nameservers, you probably want to use them as forwarders.  
    // Uncomment the following block, and insert the addresses replacing 
    // the all-0's placeholder.

    // forwarders {
    //  0.0.0.0;
    // };

    //========================================================================
    // If BIND logs error messages about the root key being expired,
    // you will need to update your keys.  See https://www.isc.org/bind-keys
    //========================================================================
    dnssec-validation no;
    allow-query { any; };
    auth-nxdomain no;    # conform to RFC1035
    listen-on { any; };
    // recursion yes;
};

            '''

        

        if filename == '/etc/bind.d/named.conf.local':
            cfg = "// Example named.conf.local file\n"
            cfg += '''
//
// Do any local configuration here
//

// here is an example for example domain (double forward slash is the line comment character)
//            zone "example.com" {
//               type master;
//               file "/etc/bind.d/db.example.com";
//            };

// for each master zone create a database file


// Consider adding the 1918 zones here, if they are not used in your
// organization
//include "/etc/bind/zones.rfc1918";
                    '''
        

        if filename == '/etc/bind.d/named.conf.default-zones':
            cfg = "// Example /etc/bind.d/named.conf.default-zones file"
            cfg += '''    
// prime the server with knowledge of the root servers
// uncomment to enable dns cache
// this would require emulation to be 
// connected to Internet
//zone "." {
//    type hint;
//    file "/etc/bind/db.root";
//};

// be authoritative for the localhost forward and reverse zones, and for
// broadcast zones as per RFC 1912

zone "localhost" {
    type master;
    file "/etc/bind/db.local";
};

zone "127.in-addr.arpa" {
    type master;
    file "/etc/bind/db.127";
};

zone "0.in-addr.arpa" {
    type master;
    file "/etc/bind/db.0";
};

zone "255.in-addr.arpa" {
    type master;
    file "/etc/bind/db.255";
};
        '''
        

        if filename == '/etc/bind.d/db.example.com':
            cfg = "; Example database file for example.com\n; Change the example.com for your domain."
            cfg += '''       
;
; BIND data file for example.com
;
$TTL    604800
@   IN  SOA ns.example.com. root.example.com. (
                  2     ; Serial
             604800     ; Refresh
              86400     ; Retry
            2419200     ; Expire
             604800 )   ; Negative Cache TTL
;
@   IN  NS  ns.example.com.
;ns  IN  A  IP_ADDRESS_OF_THIS_VIRTUAL_NODE
; replace the IP_ADDRESS_OF_THIS_VIRTUAL_NODE with the ip address of this node 
; add other records, semicolon is the comment character, remove it and change the address accordingly
; the following line will resolve www.example.com to IP_ADDRESS_OF_WEB_SERVER (you need to replace IP_ADDRESS_OF_WEB_SERVER with ip address of web server)
;www IN A IP_ADDRESS_OF_WEB_SERVER

@   IN  AAAA    ::1
                    '''

        return cfg

    @staticmethod
    def subnetentry(x):
        ''' Generate a subnet declaration block given an IPv4 prefix string
            for inclusion in the config file.
        '''
        if x.find(":") >= 0:
            # this is an IPv6 address
            return ""
        else:
            net = IPv4Prefix(x)
            return 'echo "  network %s"' % (net)

# this line is required to add the above class to the list of available services
addservice(Bind9Service)