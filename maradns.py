import os

from core.service import CoreService, addservice
from core.misc.ipaddr import IPv4Prefix, IPv6Prefix
from core.constants import *

class MaradnsService(CoreService):
    ''' This is a sample user-defined service. 
        maradns is lightweight DNS service written by Sam Trenholme.
        This module extentsion is added to Core by Sepehr Minagar.
    '''
    # a unique name is required, without spaces
    _name = "maradns"
    # you can create your own group here
    _group = "Utility"
    # list of other services this service depends on
    _depends = ()
    # per-node directories
    _dirs = ('/etc/maradns.d',)
    # generated files (without a full path this file goes in the node's dir,
    #  e.g. /tmp/pycore.12345/n1.conf/)
    _configs = ('/etc/maradns.d/mararc', 'maradns.sh',)
    # this controls the starting order vs other enabled services
    _startindex = 50
    # list of startup commands, also may be generated during startup
    _startup = ('sh maradns.sh',)
    # list of shutdown commands
    _shutdown = ()

    @classmethod
    def generateconfig(cls, node, filename, services):
        ''' Return a string that will be written to filename, or sent to the
            GUI for user customization.
        '''
        if filename == 'maradns.sh':
            cfg = "#!/bin/sh\n"
            cfg += "# auto-generated for maradns\n"
            #for ifc in node.netifs():
                #cfg += '#Node %s has interface %s\n' % (node.name, ifc.name)
                # here we do something interesting 

                #cfg += "\n".join(map(cls.subnetentry, ifc.addrlist))
                #break
             
            cfg += "maradns -f /etc/maradns.d/mararc"
    
            
        if filename == '/etc/maradns.d/mararc':
            cfg = "# Example mararc file\n"
            cfg += '''
# The address this DNS server runs on.  If you want to bind
# to multiple addresses, separate them with a comma like this:
# "10.1.2.3,10.1.2.4,127.0.0.1"
# any configuration must have at least the following four lines:

#ipv4_bind_addresses = "127.0.0.1"
#chroot_dir = "/etc/maradns.d"
#csv2 = {}
#csv2["example.com."] = "db.example.com"

# then create the zone file db.example.com in /etc/maradns.d folder
# that will contain the dns records
 
            '''
        return cfg

    @staticmethod
    def subnetentry(x):
        ''' Generate a subnet declaration block given an IPv4 prefix string
            for inclusion in the config file.
        '''
        if x.find(":") >= 0:
            # this is an IPv6 address
            return ""
        else:
            net = IPv4Prefix(x)
            return 'echo "  network %s"' % (net)

# this line is required to add the above class to the list of available services
addservice(MaradnsService)