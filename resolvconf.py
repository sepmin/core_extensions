import os

from core.service import CoreService, addservice
from core.misc.ipaddr import IPv4Prefix, IPv6Prefix
from core.constants import *

class ResolvConfService(CoreService):
    ''' To provide access to resolv.conf when IP address is static.
        This extension is added to Core by Sepehr Minagar.
    '''
    _name = "resolvconf"
    _startindex = 50
    _meta = "Adding access to a per node /etc/resolv.conf"
    _group = "Utility"
    # list of other services this service depends on
    _depends = ()
    # per-node directories
    _dirs = ()
    # generated files (without a full path this file goes in the node's dir,
    #  e.g. /tmp/pycore.12345/n1.conf/)
    _configs = ('resolvconf.sh',)
    # this controls the starting order vs other enabled services
    _startindex = 50
    # list of startup commands, also may be generated during startup
    _startup = ('sh resolvconf.sh')
    # list of shutdown commands
    _shutdown = ()

    @classmethod
    def generateconfig(cls, node, filename, services):
        ''' Return a string that will be written to filename, or sent to the
            GUI for user customization.
        '''
        if filename == 'resolvconf.sh':
            cfg = "# Add your name servers accordingly and uncomment both lines\n#Do not use with DHCPClient if DHCP server specifies DNS address\n"
            cfg += '#mkdir -p /var/run/resolvconf\n#echo "nameserver 10.0.0.1" > /var/run/resolvconf/resolv.conf'

        return cfg

addservice(ResolvConfService)