import os

from core.service import CoreService, addservice
from core.misc.ipaddr import IPv4Prefix, IPv6Prefix
from core.constants import *

class strongswan(CoreService):
    ''' This is a sample user-defined service. 
        
        This module extentsion is added to Core by Sepehr Minagar.
    '''
    # a unique name is required, without spaces
    _name = "strongswan"
    # you can create your own group here
    _group = "Extensions"
    # list of other services this service depends on
    _depends = ()
    # per-node directories
    _dirs = ('/etc/ipsec.d/core', '/etc/strongswan.d/core',)
    # generated files (without a full path this file goes in the node's dir,
    #  e.g. /tmp/pycore.12345/n1.conf/)
    _configs = ('/etc/ipsec.d/core/ipsec.conf', '/etc/ipsec.d/core/ipsec.secrets', '/etc/strongswan.d/core/strongswan.conf', 'ipsec.sh')
    # this controls the starting order vs other enabled services
    _startindex = 50
    # list of startup commands, also may be generated during startup
    _startup = ('sh ipsec.sh',)
    # list of shutdown commands
    _shutdown = ('ipsec stop')

    @classmethod
    def generateconfig(cls, node, filename, services):
        ''' Return a string that will be written to filename, or sent to the
            GUI for user customization.
        '''
       
        if filename == 'ipsec.sh':
            cfg = "#!/bin/sh\n"
            cfg += "# auto-generated for strongswan\n"
            #
             
            cfg += '''
ipsec start

# the starting up may take some time
# uncomment the following line if
# IPSec SAs are established but
# CHILD_SAs are not
#sleep 5s

# uncomment the follwoing lines and replace conn_name with 
# the name of connection defined in ipsec.conf
#ipsec up conn_name
#ipsec route conn_name

'''    
            
        

        if filename == '/etc/ipsec.d/core/ipsec.conf':
            cfg = "# auto-generated for strongswan\n"
            cfg += '''
# check out man ipsec.conf for configuration options
'''

        if filename == '/etc/ipsec.d/core/ipsec.secrets':
            cfg = "# auto-generated for strongswan\n"
            cfg += '''
# check out man ipsec.secrets for configuration options
'''
        
        if filename == '/etc/strongswan.d/core/strongswan.conf':
            cfg = "# auto-generated for strongswan\n"
            cfg += '''
# place configuration specific to strongswan application in this file
# check out man strongswan.conf for configuration options
''' 
        


        

        return cfg

    @staticmethod
    def subnetentry(x):
        ''' Generate a subnet declaration block given an IPv4 prefix string
            for inclusion in the config file.
        '''
        if x.find(":") >= 0:
            # this is an IPv6 address
            return ""
        else:
            net = IPv4Prefix(x)
            return 'echo "  network %s"' % (net)

# this line is required to add the above class to the list of available services
addservice(strongswan)